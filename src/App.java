import com.devcamp.Author;
import com.devcamp.Book;
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Author author1 = new Author("Huy Nguyễn 18+", 'm');

        Author author2 = new Author("Yến Vũ Children", 'l');

        System.out.println(author1);
        System.out.println(author2);
            //khoi tao book1 (không qty) va book2 (có qty)
        Book book1 = new Book ("Ông Lão Đánh Cá và Con Cá Vàng", author1, 100000);
        Book book2 = new Book ("Có Cá Vàng nhậu ông Già bị THỔI NỒNG ĐỘ CỒN", author2, 100000, 5);

        System.out.println(book1);
        System.out.println(book2);
    }
}
